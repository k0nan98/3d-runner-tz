using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleStick : MonoBehaviour
{
    public bool isContacted = false;
    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Stick")
        {
            isContacted = true;
            //CompositionRoot.sharedInstance.GetPlayer().StopParticles();
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Stick")
        {
            isContacted = false;
            //CompositionRoot.sharedInstance.GetPlayer().StopParticles();
        }
    }
}
