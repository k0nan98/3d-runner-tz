using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleController : MonoBehaviour
{
    [SerializeField] HoleStick leftStick;
    [SerializeField] HoleStick rightStick;
    void Start()
    {
        
    }

    float time = 0;
    void FixedUpdate()
    {
        if ((leftStick.isContacted && !rightStick.isContacted) || (!leftStick.isContacted && rightStick.isContacted))
        {
            time += Time.fixedDeltaTime;
        }
        else time = 0;
        PlayerController player = CompositionRoot.sharedInstance.GetPlayer();
        player.StartParticles(leftStick.isContacted, rightStick.isContacted);
        if (leftStick.isContacted || rightStick.isContacted)
        {
            CompositionRoot.sharedInstance.GetPlayer().GetComponent<Rigidbody>().AddForce(Vector3.down * 2);
        }

        if (time > 0.5f && ((leftStick.isContacted && !rightStick.isContacted) || (!leftStick.isContacted && rightStick.isContacted)))
        {
            if (leftStick.isContacted)
            {
                CompositionRoot.sharedInstance.GetStickController().RemoveSegments(StickSegment.SegmentDirection.Left, 1);
            }
            if (rightStick.isContacted)
            {
                CompositionRoot.sharedInstance.GetStickController().RemoveSegments(StickSegment.SegmentDirection.Right, 1);
            }
        }
    }
}
