using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNewController : MonoBehaviour
{
    bool run = false;
    public Transform nextAngle;
    public List<Transform> lvlGenrPoses = new List<Transform>();
    public Animator myAnimator;
    public Transform target;
    public Transform waterLevel;

    public Transform endPoint;
    public static PlayerNewController instance;

    public float health;
    public float maxHealth;
    public void StartGameplay()
    {
        //run = true;
    }
    void Start()
    {
        run = true;
        instance = this;
        cameraTargetPos = Camera.main.transform.position;

        StartCoroutine(PlayerSpeedSlowdown());
        StartCoroutine(CameraMotion());
        StartCoroutine(PlayerMotion());

    }

    public int angleId = 0;
    Vector3 cameraTargetPos = Vector3.zero;

    Vector3 newTargetPos = Vector3.zero;

    Vector3 mouseA = Vector3.zero, mouseB = Vector3.zero;
    Vector3 oldLook = Vector3.zero;
    void Update()
    {

        if (health > maxHealth)
        {
            health = maxHealth;
        }
        else if (health < 0)
        {
            StopAllCoroutines();
            this.enabled = false;
        }
        this.transform.localScale = Vector3.one * health / 100;
        //waterLevel.transform.position = new Vector3(waterLevel.position.x, this.transform.position.y - 2, waterLevel.position.z);



        if (angleId + 1 < lvlGenrPoses.Count)
        {

            Vector3 v2 = (this.transform.position - (lvlGenrPoses[angleId].transform.position + this.transform.right*2)).normalized;

            newTargetPos = this.transform.position - v2 * 40;
            newTargetPos.y = this.transform.position.y;
            if (Vector3.Distance(target.transform.position, newTargetPos) > 1)
                target.transform.position -= (target.transform.position - newTargetPos).normalized * Time.deltaTime * 10;
            else run = true;
        }
        if (run)
        {
            target.parent = null;
            if (moveTarget == Vector3.zero)
            {
                //moveTarget = lvlGenrPoses[angleId].transform.position;

            }

            if (oldLook == Vector3.zero)
            {
                
                oldLook = Camera.main.transform.position - (Camera.main.transform.position - (this.transform.position + Vector3.up * 5)).normalized * 1000;
                Camera.main.transform.LookAt(target);
            } else
            {
                Vector3 t = Camera.main.transform.position - (Camera.main.transform.position - (this.transform.position + Vector3.up * 5)).normalized * 1000 ;

                //oldLook = t;
                if(Vector3.Distance(t, oldLook) > 3)
                oldLook -= (oldLook - t).normalized*Time.deltaTime*300;
                else 
                oldLook -= (oldLook - t).normalized*Time.deltaTime*50;
                Camera.main.transform.LookAt(target);
            }


            if (Input.GetMouseButtonDown(0))
            {
                mouseA = Input.mousePosition;
            }
            else
            if (Input.GetMouseButton(0))
            {
                mouseB = (Input.mousePosition - mouseA).normalized;
            }
            else
            {
                mouseB = Vector3.zero;
            }
#if !UNITY_EDITOR
            if(Input.touchCount > 0)
            {
                if(Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    mouseA = Input.mousePosition;
                }
                if(Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    mouseB = (Input.mousePosition - mouseA).normalized;
                } else if(Input.GetTouch(0).phase == TouchPhase.Ended) mouseB = Vector3.zero;
            } else mouseB = Vector3.zero;

#endif

            Debug.Log("Run" + angleId);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == lvlGenrPoses[angleId].gameObject.name)
        {
            angleId++;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == lvlGenrPoses[angleId].gameObject.name)
        {
            angleId++;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
            if (collision.gameObject.name == lvlGenrPoses[angleId].gameObject.name)
            {
                angleId++;
            }
    }
    private void OnCollisionStay(Collision collision)
    {
            if (collision.gameObject.name == lvlGenrPoses[angleId].gameObject.name)
            {
                angleId++;
            }
    }
    public float m_speed =8f;
    float minSpeed = 8f;
    public Vector3 moveTarget = Vector3.zero;
    public float Controll_X_Sens = 2;
    public Rigidbody m_rig;
    IEnumerator PlayerMotion()
    {
        while (true)
        {
            m_rig.velocity = Vector3.zero;
            m_rig.angularVelocity = Vector3.zero;
            //target.transform.position = this.transform.position - v2 * 10;
           
                myAnimator.transform.LookAt(target);

            if (lvlGenrPoses.Count > angleId)
            {
                //-------------------------
                moveTarget -= (moveTarget - lvlGenrPoses[angleId].transform.position).normalized * Time.deltaTime * 12;
                Vector3 nextPos = (this.transform.position - moveTarget).normalized;

                if (Physics.Raycast(this.transform.position + myAnimator.transform.right * Mathf.Sign(mouseB.x) * 0.5f, Vector3.down, 100))
                    m_rig.MovePosition(this.transform.position - ((myAnimator.transform.forward * -1 + nextPos).normalized + myAnimator.transform.right * mouseB.x * (Controll_X_Sens * -1)).normalized * Time.deltaTime * m_speed);
                //this.transform.position = this.transform.position - ((myAnimator.transform.forward * -1 + nextPos).normalized + myAnimator.transform.right * mouseB.x * (Controll_X_Sens * -1)).normalized * Time.deltaTime * m_speed;
                else m_rig.MovePosition(this.transform.position = this.transform.position - (myAnimator.transform.forward * -1 + nextPos).normalized * Time.deltaTime * m_speed);
                    //this.transform.position = this.transform.position - (myAnimator.transform.forward * -1 + nextPos).normalized * Time.deltaTime * m_speed;
                //------------------------
                Vector3 v = (this.transform.position - lvlGenrPoses[angleId].transform.position).normalized;
                //Vector3 nc = this.transform.position + v * 10;
                Ray r = new Ray(this.transform.position, (this.transform.position - target.position).normalized);
                Vector3 c = r.GetPoint(10);
                cameraTargetPos = cameraTargetPos - (cameraTargetPos - new Vector3(c.x, this.transform.position.y +3, c.z)).normalized * Time.deltaTime * 15;
                


            }
            
            
            yield return new WaitForEndOfFrame();
        }

    }
    IEnumerator PlayerSpeedSlowdown()
    {
        while (true)
        {
            if (m_speed > minSpeed)
            {
                m_speed -= Time.deltaTime;
            }
            else m_speed = minSpeed;
            yield return new WaitForEndOfFrame();
        }
    }
    IEnumerator CameraMotion()
    {

        while (true)
        {

            if (Vector3.Distance(Camera.main.transform.position, cameraTargetPos) > 1)
            {
                Vector3 l = Camera.main.transform.position-(Camera.main.transform.position - (cameraTargetPos)).normalized * Time.deltaTime * 9;
                Camera.main.transform.position = l;
                //Camera.main.transform.position = Vector3.LerpUnclamped(Camera.main.transform.position, l, 0.5f);
            }

            yield return new WaitForEndOfFrame();
        }

    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
