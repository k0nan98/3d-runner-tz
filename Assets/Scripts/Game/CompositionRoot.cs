using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompositionRoot : MonoBehaviour
{
    [SerializeField] private PlayerController player;
    [SerializeField] private Stick stickController;
    public static CompositionRoot sharedInstance;
    public void Awake()
    {
        sharedInstance = this;
    }
    public PlayerController GetPlayer()
    {
        return player;
    }
    public Stick GetStickController()
    {
        return stickController;
    }
}
