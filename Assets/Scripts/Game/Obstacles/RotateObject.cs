using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public static int obj = 0;
    public bool needRandom = false;
    private void OnEnable()
    {
        if (needRandom)
        {
            this.transform.parent = null;
            obj++;
            if (obj % 6 != 0 && obj != 1)
                this.gameObject.SetActive(false);
            Debug.Log("obj:" + obj);
        }
    }
    private void Awake()
    {
        
    }

    public Vector3 rotateVector;
    void Update()
    {
        this.transform.Rotate(rotateVector * Time.deltaTime*80);
    }
}
