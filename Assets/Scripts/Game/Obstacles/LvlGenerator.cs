using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlGenerator : MonoBehaviour
{
    [SerializeField]
    public List<lvlObject> poolBasic;

    private float roadLength;
    public float roadLengthMustBe;
    public int anglesCount;
    public Transform startPoint;
    int forwards = 0;

    Vector3 playerSpawner = Vector3.zero;
    public PlayerController player;
    void Start()
    {
        roadFragment roadfragment = roadFragment.AngleLeft;

        roadFragment side = roadFragment.Forward;
        playerSpawner = startPoint.position + Vector3.up * 2 + Vector3.left*2;

        while (roadLength < roadLengthMustBe)
        {
            if (forwards > 3)
            {
                if(roadfragment == roadFragment.Forward)
                roadfragment = (roadFragment)UnityEngine.Random.Range(0, 3);
                else if(roadfragment == roadFragment.AngleLeft)
                {
                    if (UnityEngine.Random.value < 0.5f) roadfragment = roadFragment.Forward;
                    else roadfragment = roadFragment.AngleLeft;
                }
                else if (roadfragment == roadFragment.AngleRight)
                {
                    if (UnityEngine.Random.value < 0.5f) roadfragment = roadFragment.Forward;
                    else roadfragment = roadFragment.AngleRight;
                }
                if (roadfragment != side)
                forwards = 0;
            }
            else roadfragment = side;

            List<lvlObject> forwardRoads = new List<lvlObject>();
            List<lvlObject> leftAngles = new List<lvlObject>();
            List<lvlObject> rightAngles = new List<lvlObject>();
            List<lvlObject> upAngles = new List<lvlObject>();
            foreach(lvlObject obj in poolBasic)
            {
                if(obj.road == roadFragment.Forward)
                {
                    forwardRoads.Add(obj);
                } else if(obj.road == roadFragment.AngleLeft)
                {
                    leftAngles.Add(obj);
                } else if(obj.road == roadFragment.AngleRight)
                {
                    rightAngles.Add(obj);
                } else if(obj.road == roadFragment.Up)
                {
                    upAngles.Add(obj);
                }
            }
            forwards++;
            if (roadfragment == roadFragment.Forward)
            {
                side = roadFragment.Forward;
                spawnForward(forwardRoads[UnityEngine.Random.Range(0, forwardRoads.Count)], upAngles.ToArray());
            } else
            {
                side = roadfragment;
                if (forwards != 1)
                {
                    if (roadfragment == roadFragment.AngleLeft)
                        spawnLeft(forwardRoads[UnityEngine.Random.Range(0, forwardRoads.Count)]);
                    if (roadfragment == roadFragment.AngleRight)
                        spawnRight(forwardRoads[UnityEngine.Random.Range(0, forwardRoads.Count)]);
                } else
                {
                    if (roadfragment == roadFragment.AngleLeft)
                        spawnLeft(leftAngles.ToArray(), true);
                    if (roadfragment == roadFragment.AngleRight)
                        spawnRight(rightAngles.ToArray(), true);
                    //�������� ������� �����
                }
            }

        }
        player.transform.position = playerSpawner;
        //player.StartGameplay();
    }
    public List<GameObject> RoadAngles = new List<GameObject>();
    public List<GameObject> AllRoad = new List<GameObject>();
    void spawnLeft(lvlObject itm)
    {
        startPoint.eulerAngles = Vector3.up * -90;
        startPoint.transform.position -= startPoint.right * itm.nextOffset.x;

        roadLength += itm.nextOffset.x + itm.nextOffset.y + itm.nextOffset.z;
        GameObject obj = GameObject.Instantiate(itm.go, startPoint.position, startPoint.rotation, startPoint);
        obj.transform.parent = null;

        //RoadAngles.Add(obj);
        AllRoad.Add(obj);
    }
    void spawnRight(lvlObject itm)
    {
        startPoint.eulerAngles = Vector3.up * 90;
        startPoint.transform.position -= startPoint.right * itm.nextOffset.x;

        roadLength += itm.nextOffset.x + itm.nextOffset.y + itm.nextOffset.z;
        GameObject obj = GameObject.Instantiate(itm.go, startPoint.position, startPoint.rotation, startPoint);
        obj.transform.parent = null;
        //RoadAngles.Add(obj);
        AllRoad.Add(obj);

    }
    void spawnLeft(lvlObject[] itms, bool isAngle)
    {
        forwardsFromUp -= 2;
        lvlObject itm = itms[UnityEngine.Random.Range(0, itms.Length)];
        if (isAngle)
        {
            startPoint.eulerAngles = Vector3.up * -90;
            startPoint.transform.position -= startPoint.right * itm.nextOffset.x;

            roadLength += itm.nextOffset.x + itm.nextOffset.y + itm.nextOffset.z;
            GameObject obj = GameObject.Instantiate(itm.go, startPoint.position, startPoint.rotation, startPoint);
            obj.transform.parent = null;
            AllRoad.Add(obj);
        }
        else spawnLeft(itm);

    }
    void spawnRight(lvlObject[] itms, bool isAngle)
    {
        forwardsFromUp -= 2;
        lvlObject itm = itms[UnityEngine.Random.Range(0, itms.Length)];
        if (isAngle)
        {
            startPoint.eulerAngles = Vector3.up * 90;
            startPoint.transform.position -= startPoint.right * itm.nextOffset.x;

            roadLength += itm.nextOffset.x + itm.nextOffset.y + itm.nextOffset.z;
            GameObject obj = GameObject.Instantiate(itm.go, startPoint.position, startPoint.rotation, startPoint);
            obj.transform.parent = null;
            AllRoad.Add(obj);
        }
        else spawnRight(itm);

    }
    int forwardsFromUp = 0;
    void spawnForward(lvlObject itm, lvlObject[] ups)
    {
        forwardsFromUp++;
        if (forwardsFromUp < 5)
        {
            startPoint.eulerAngles = Vector3.zero;
            startPoint.transform.position -= startPoint.right * itm.nextOffset.x;

            roadLength += itm.nextOffset.x + itm.nextOffset.y + itm.nextOffset.z;
            GameObject obj = GameObject.Instantiate(itm.go, startPoint.position, startPoint.rotation, startPoint);
            obj.transform.parent = null;
            AllRoad.Add(obj);
        } else
        {
            forwardsFromUp = 0;
            spawnUp(ups);
        }

    }
    void spawnUp(lvlObject[] ups)
    {
        forwards = 1;
        lvlObject itm = ups[UnityEngine.Random.Range(0, ups.Length)];
        startPoint.eulerAngles = Vector3.zero;
        startPoint.transform.position -= startPoint.right * itm.nextOffset.x;

        roadLength += itm.nextOffset.x + itm.nextOffset.y + itm.nextOffset.z;
        GameObject obj = GameObject.Instantiate(itm.go, startPoint.position, startPoint.rotation, startPoint);
        startPoint.transform.position += Vector3.up * itm.nextOffset.y;
        startPoint.transform.position -= startPoint.right * itm.nextOffset.x;
        obj.transform.parent = null;

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
[Serializable]
public class lvlObject
{
    public GameObject go;
    public roadFragment road;
    public Vector3 curOffset;
    public Vector3 nextOffset;
}
public enum roadFragment
{
    Forward,
    AngleLeft,
    AngleRight,
    Up
};