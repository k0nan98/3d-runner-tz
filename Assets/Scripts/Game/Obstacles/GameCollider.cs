using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCollider : MonoBehaviour
{
    public int health;
    public colType type;
    public enum colType
    {
        Heal,
        Fly,
        Damage,
        Speed,
        Slow,
        Restart
    };

    private void OnCollisionEnter(Collision collision)
    {


        if (collision.collider.gameObject.tag == "Player")
        {
            PlayerController player = CompositionRoot.sharedInstance.GetPlayer();
            if (type == colType.Speed)
            {
                player.setSpeed(12f);
            }
            else
            if (type == colType.Slow)
            {
                player.setSpeed(4f);
            }
            else
            {
                player.setSpeed(9f);
            }
            if (type == colType.Restart) UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            if (type == colType.Heal)
            {
                Stick stickController = CompositionRoot.sharedInstance.GetStickController();
                stickController.AddSegments(health);
                Destroy(this.gameObject);
            }
            if (type == colType.Fly)
            {
                player.getAnimator().SetBool("Fly", true);
                player.setInputState(false);
                Stick stick = CompositionRoot.sharedInstance.GetStickController();

                stick.GetCounts(out int left, out int right);
                if (left > health && right > health)
                {
                    player.StartParticles();
                    player.transform.position = new Vector3(this.transform.position.x, player.transform.position.y, player.transform.position.z);
                }
                else
                {
                    this.gameObject.SetActive(false);
                }
            }
        }
        
        
        if (collision.collider.gameObject.tag == "Stick")
        {
            if (type == colType.Damage)
            {
                Stick stickController = CompositionRoot.sharedInstance.GetStickController();
                if (collision.collider.gameObject.TryGetComponent<StickSegment>(out StickSegment segment))
                {
                    stickController.RemoveSegments(segment.direction, segment.ID);
                }
            }
        }

    }
    float damageTime = 0;
    private void OnCollisionStay(Collision collision)
    {
        if(collision.collider.tag == "Player")
        {
            if (type == colType.Damage)
            {
                damageTime += Time.fixedDeltaTime;
                if (damageTime > 0.2f)
                {
                    Stick stickController = CompositionRoot.sharedInstance.GetStickController();
                    stickController.RemoveSegments(StickSegment.SegmentDirection.Left, 0, true);
                    stickController.RemoveSegments(StickSegment.SegmentDirection.Right, 0, true);
                    damageTime = 0;
                }
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        PlayerController player = CompositionRoot.sharedInstance.GetPlayer();
        player.getAnimator().SetBool("Fly", false);
        player.StopParticles();
        player.setInputState(true);
    }
}
