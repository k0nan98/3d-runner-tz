using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoadFinish : MonoBehaviour
{
    public Animator finishAnim;
    public Transform player;
    void Start()
    {

    }
    bool started = false;
    private void OnCollisionStay(Collision collision)
    {
        if(!started) Camera.main.gameObject.SetActive(false);
        if (collision.gameObject.tag == "Player")
        {
            PlayerController player = CompositionRoot.sharedInstance.GetPlayer();
            if (player != null)
            {
                player.enabled = false;
                player.StopAllCoroutines();
                player.gameObject.SetActive(false);
            }
            finishAnim.enabled = true;
        }
    }
    public void ReloadLvl()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public static void sReloadLvl()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
