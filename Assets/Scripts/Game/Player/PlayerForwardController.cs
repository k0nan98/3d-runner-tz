using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerForwardController : PlayerController
{
    [SerializeField] private Vector3 moveTarget = Vector3.zero;
    [SerializeField] private float Controll_X_Sens = 2;

    [SerializeField] private Animator myAnimator;
    [SerializeField] private Transform target;
    [SerializeField] List<ParticleSystem> particles;

    public float m_speed = 6.5f;

    Vector3 mouseA = Vector3.zero, mouseB = Vector3.zero;
    private bool _isInputActive = true;
    private bool _isDead = false;

    private Vector3 offsetCamera = Vector3.zero;
    private float minSpeed = 8;
    void Start()
    {
        minSpeed = m_speed;
        offsetCamera = this.transform.position-Camera.main.transform.position;

        StartCoroutine(PlayerMotion());
        StartCoroutine(PlayerSpeedSlowdown());
        StartCoroutine(PlayerSpeedUp());
    }
    void Update()
    {


        if (_isDead)
        {
            StopAllCoroutines();
            this.enabled = false;
            RoadFinish.sReloadLvl();
        }
        target.transform.position = this.transform.position + Vector3.forward*10;

        if (!_isInputActive)
        {
            mouseA = Vector3.zero;
            mouseB = Vector3.zero;
            return;
        }
            if (Input.GetMouseButtonDown(0))
            {
                mouseA = Input.mousePosition;
            }
            else
           if (Input.GetMouseButton(0))
            {
                mouseB = (Input.mousePosition - mouseA).normalized;
            }
            else
            {
                mouseB = Vector3.zero;
            }
#if !UNITY_EDITOR
            if(Input.touchCount > 0)
            {
                if(Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    mouseA = Input.mousePosition;
                }
                if(Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    mouseB = (Input.mousePosition - mouseA).normalized;
                } else if(Input.GetTouch(0).phase == TouchPhase.Ended) mouseB = Vector3.zero;
            } else mouseB = Vector3.zero;

#endif
            return;


    }
    public override void StartParticles()
    {
        foreach (ParticleSystem particle in particles)
        {
            particle.Play();
        }
    }
    public override void StartParticles(bool left, bool right)
    {
        if (left)
            particles[0].Play();
        if (right)
            particles[1].Play();
    }
    public override void StopParticles()
    {
        foreach (ParticleSystem particle in particles)
        {
            particle.Stop();
        }
    }

    IEnumerator PlayerSpeedSlowdown()
    {
        while (true)
        {
            if (m_speed > minSpeed)
            {
                m_speed -= Time.deltaTime;
            }
            //else m_speed = minSpeed;
            yield return new WaitForEndOfFrame();
        }
    }
    IEnumerator PlayerSpeedUp()
    {
        while (true)
        {
            if (m_speed < minSpeed)
            {
                m_speed += Time.deltaTime;
            }
            //else m_speed = minSpeed;
            yield return new WaitForEndOfFrame();
        }
    }
    IEnumerator PlayerMotion()
    {
        while (true)
        {

            myAnimator.transform.LookAt(target);
            moveTarget -= (moveTarget - target.transform.position).normalized * Time.deltaTime * 25;
            Vector3 nextPos = (this.transform.position - moveTarget).normalized;

            if (Physics.Raycast(this.transform.position + myAnimator.transform.right * Mathf.Sign(mouseB.x) * 0.5f, Vector3.down, 100))
                this.transform.position = this.transform.position - ((myAnimator.transform.forward * -1 + nextPos).normalized + myAnimator.transform.right * mouseB.x * (Controll_X_Sens * -0.22f)).normalized * Time.deltaTime * m_speed;
            else this.transform.position = this.transform.position - (myAnimator.transform.forward * -1 + nextPos).normalized * Time.deltaTime * m_speed;
            this.transform.position += this.GetComponent<Rigidbody>().velocity * Time.deltaTime;


            Camera.main.transform.position = new Vector3(this.transform.position.x - offsetCamera.x, Vector3.LerpUnclamped(Camera.main.transform.position, this.transform.position - offsetCamera, Time.deltaTime).y, this.transform.position.z - offsetCamera.z);

            yield return new WaitForEndOfFrame();
        }

    }

    public override void setSpeed(float speed)
    {
        m_speed = speed;
    }

    public override Animator getAnimator()
    {
        return myAnimator;
    }

    public override void setHealth(float hp)
    {
        //health = hp;
    }

    public override float getHealth()
    {
        //return health;
        return 0;
    }

    public override void setInputState(bool isInputActive)
    {
        _isInputActive = isInputActive;
    }
}
