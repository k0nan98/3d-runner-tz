using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickSegment : MonoBehaviour
{
    public int ID;
    public enum SegmentDirection
    {
        Left,
        Right
    }
    public SegmentDirection direction;
    [SerializeField] private GameObject particlePrefab;

    public void StickDestroy()
    {
        GameObject temp = Instantiate(particlePrefab, this.transform.position, this.transform.rotation, null);
        temp.GetComponent<Rigidbody>().AddForce(Vector3.back);
    }
}
