using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    [SerializeField] private Transform[] hands;
    [SerializeField] private StickSegment segment;
    List<StickSegment> segmentsToLeft = new List<StickSegment>();
    List<StickSegment> segmentsToRight = new List<StickSegment>();
    private void Awake()
    {
        Generate(5, 5);
    }
    public void GetCounts(out int left, out int right)
    {
        left = segmentsToLeft.Count;
        right = segmentsToRight.Count;
    }
    public void Generate(int toLeft, int toRight)
    {
        foreach (StickSegment segment in segmentsToLeft)
            Destroy(segment.gameObject);
        foreach (StickSegment segment in segmentsToRight)
            Destroy(segment.gameObject);
        segmentsToLeft.Clear();
        segmentsToRight.Clear();

        for(int i = 0; i<toLeft; i++)
        {
            StickSegment temp = Instantiate(segment, this.transform);
            temp.ID = segmentsToLeft.Count;
            temp.direction = StickSegment.SegmentDirection.Left;
            segmentsToLeft.Add(temp);
            temp.transform.localScale = Vector3.one;
            temp.transform.localPosition += Vector3.left * i;
        }
        for (int i = 0; i < toRight; i++)
        {
            StickSegment temp = Instantiate(segment, this.transform);
            temp.ID = segmentsToRight.Count;
            temp.direction = StickSegment.SegmentDirection.Right;
            segmentsToRight.Add(temp);
            temp.transform.localScale = Vector3.one;
            temp.transform.localPosition += Vector3.right * i;
        }

    }
    public void AddSegments(int count)
    {
        if (count <= 0) return;
        int perDirection = count % 2 == 0 ? count / 2 : (int)(count/2)+1;
        Generate(segmentsToLeft.Count + count, segmentsToRight.Count + count);
    }
    public void RemoveSegments(StickSegment.SegmentDirection from, int startId, bool fromEnd = false)
    {
        if (!fromEnd)
        {
            if (from == StickSegment.SegmentDirection.Left && startId < segmentsToLeft.Count)
            {
                List<StickSegment> toDestroy = segmentsToLeft.FindAll(x => x.ID >= startId);
                segmentsToLeft.RemoveAll(x => x.ID >= startId);
                foreach (StickSegment stickSegment in toDestroy)
                {
                    stickSegment.StickDestroy();
                    Destroy(stickSegment.gameObject);
                }
            }
            if (from == StickSegment.SegmentDirection.Right && startId < segmentsToRight.Count)
            {
                List<StickSegment> toDestroy = segmentsToRight.FindAll(x => x.ID >= startId);
                segmentsToRight.RemoveAll(x => x.ID >= startId);
                foreach (StickSegment stickSegment in toDestroy)
                {
                    stickSegment.StickDestroy();
                    Destroy(stickSegment.gameObject);
                }
            }
        } else
        {
            if (from == StickSegment.SegmentDirection.Left && startId < segmentsToLeft.Count)
            {
                if (segmentsToLeft.Count <= 1) return;
                List<StickSegment> toDestroy = segmentsToLeft.FindAll(x => x.ID >= segmentsToLeft[segmentsToLeft.Count-startId-1].ID);
                segmentsToLeft.RemoveAll(x => x.ID >= segmentsToLeft[segmentsToLeft.Count - startId - 1].ID);
                foreach (StickSegment stickSegment in toDestroy)
                {
                    stickSegment.StickDestroy();
                    Destroy(stickSegment.gameObject);
                }
            }
            if (from == StickSegment.SegmentDirection.Right && startId < segmentsToRight.Count)
            {
                if (segmentsToRight.Count <= 1) return;
                List<StickSegment> toDestroy = segmentsToRight.FindAll(x => x.ID >= segmentsToRight[segmentsToRight.Count - startId - 1].ID);
                segmentsToRight.RemoveAll(x => x.ID >= segmentsToRight[segmentsToRight.Count - startId - 1].ID);
                foreach (StickSegment stickSegment in toDestroy)
                {
                    stickSegment.StickDestroy();
                    Destroy(stickSegment.gameObject);
                }
            }
            
        }
    }

    void Update()
    {
        this.transform.position = (hands[0].position + hands[1].position) * 0.5f;
    }
}
