﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;

public class ScreenShotter : MonoBehaviour
{
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    static int counter = 0;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            /*            //iphone
            screenShot(1242, 2688);
            screenShot(1125, 2436);
            screenShot(1242, 2208);
            screenShot(750, 1334);
            screenShot(640, 1096);
            screenShot(640, 1136);
            screenShot(640, 920);
            screenShot(640, 960);
            //ipad
            screenShot(2048, 2732);
            screenShot(2048, 2732);
            screenShot(1668, 2388);
            screenShot(1668, 2224);
            screenShot(1536, 2008);
            screenShot(1536, 2048);
            */
            //iphone
            screenShot(1440, 2560);


            counter++;
        }
    }



    public static string ScreenShotName(int width, int height)
    {
        return string.Format("{0}/screenshots/" + width + "x" + height + "/screen" + SceneManager.GetActiveScene().name + "_{1}x{2}_{3}" + counter + ".png",
                             Application.dataPath,
                             width, height,
                             System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    void screenShot(int w, int h)
    {
        RenderTexture rt = new RenderTexture(w, h, 24);
        Camera.main.targetTexture = rt;
        Texture2D screenShot = new Texture2D(w, h, TextureFormat.RGB24, false);
        Camera.main.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, w, h), 0, 0);
        Camera.main.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = ScreenShotName(w, h);
        if (!Directory.Exists("Assets/Screenshots/" + w + "x" + h + "/"))
            Directory.CreateDirectory("Assets/Screenshots/" + w + "x" + h + "/");
        System.IO.File.WriteAllBytes(filename, bytes);
    }
}
#endif