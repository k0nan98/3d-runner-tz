using UnityEngine;
public abstract class PlayerController : MonoBehaviour
{
    public abstract void setSpeed(float speed);
    public abstract void setHealth(float hp);
    public abstract void setInputState(bool isInputActive);
    public abstract float getHealth();
    public abstract Animator getAnimator();
    public abstract void StartParticles();
    public abstract void StartParticles(bool left, bool right);
    public abstract void StopParticles();

}
